﻿using System.Threading.Tasks;
using DDDWay.Application;
using Microsoft.AspNetCore.Mvc;
using DDDWay.Application.Messaging;

namespace DDDWay.WebAPI.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            this._customerService = customerService;
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/customers")]
        public async Task<ActionResult> GetAllCustomers()
        {
            return Ok(await _customerService.GetAllCustomers());
        }

        /// <summary>
        /// Get customer by Id
        /// </summary>
        /// <param name="customerRequest"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/customers/{CustomerId}")]
        public async Task<ActionResult> GetCustomer([FromRoute]CustomerRequest customerRequest)
        {
            return Ok(await _customerService.GetAllCustomers());
        }

        /// <summary>
        /// Get customer by Id
        /// </summary>
        /// <param name="customerRequest"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/customers/filter/{CustomerId}/{Code}")]
        public async Task<ActionResult> GetCustomerForFilter([FromRoute]CustomerRequest customerRequest)
        {
            return Ok(await _customerService.GetAllCustomers());
        }
    }
}
