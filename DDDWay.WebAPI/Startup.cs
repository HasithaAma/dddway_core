﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System.Reflection;
using DDDWay.Application;
using Swashbuckle.AspNetCore.Swagger;
using DDDWay.Domain;
using AutoMapper;
using DDDWay.Infrastructure.Repository;
using DDDWay.Infrastructure.ExceptionHandling;
using DDDWay.Infrastructure.Logging;

namespace DDDWay.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Logger configuration
            var logger = new LoggerConfiguration()
                 .ReadFrom.Configuration(Configuration)
                 .WriteTo.Trace()
                 .Enrich.FromLogContext()
                 .Enrich.WithMachineName()
                 .Enrich.WithProperty("Version", Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion)
                 .CreateLogger();

            services.AddMvc();

            // Swagger stuff
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "EDI Profile Manager Service API", Version = "v1" });

                c.IncludeXmlComments($"{ System.AppDomain.CurrentDomain.BaseDirectory }\\{Assembly.GetEntryAssembly().GetName().Name}.xml");
                c.DescribeAllEnumsAsStrings();
            });

            // Add DBContext
            services.AddDbContext<DDDWayDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("DDDWayConnectionString")));

            services.AddSingleton<Serilog.ILogger>(logger);
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<Serilog.ILogger>(logger);
            services.AddAutoMapper();

            services.AddMvc(options => options.Filters.Add(new GlobalExceptionFilter()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseSwagger();
            app.UseSwaggerUI(x =>
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "EDI Profile Manager Service API V1"));

            app.UseMiddleware<SerilogMiddleware>();

            app.UseMvc();
        }
    }
}
