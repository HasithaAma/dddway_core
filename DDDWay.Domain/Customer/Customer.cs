﻿using System.Text;
using System.Linq;
using DDDWay.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDDWay.Domain
{
    /// <summary>
    /// Domain entity for the 
    /// </summary>
    public class Customer : EntityBase<int>, IAggregateRoot
    {
        #region Domain logic

        //All domain entity validations go here
        protected override void ValidateAndThrowIfDomainException()
        {
            if (string.IsNullOrEmpty(Code))
            {
                AddBrokenRule(CustomerBusinessRule.CustomerNameRequired);
            }

            if (string.IsNullOrEmpty(Code))
            {
                AddBrokenRule(CustomerBusinessRule.CustomerCodeRequired);
            }

            if (this.BrokenRules.Count() > 0)
            {
                StringBuilder issues = new StringBuilder();
                foreach (BusinessRule businessRule in this.BrokenRules)
                {
                    issues.AppendLine(businessRule.RuleDescription);
                }

                throw new DomainException(issues.ToString());
            }
        }

        #endregion

        #region ORM Fields

        public int CustomerId { get; private set; }

        public string Code { get; set; }

        #endregion

    }

    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(x => x.CustomerId);
        }
    }
}
