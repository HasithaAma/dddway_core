﻿using DDDWay.Domain;

namespace DDDWay.Domain
{
    public class CustomerBusinessRule
    {
        public static readonly BusinessRule CustomerNameRequired = new BusinessRule("A customer must have a name.");
        public static readonly BusinessRule CustomerCodeRequired = new BusinessRule("A customer must have a code.");
    }
}
