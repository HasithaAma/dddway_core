﻿using DDDWay.Domain;

namespace DDDWay.Domain
{
    public interface ICustomerRepository : IRepository<Customer, int>
    {
    }
}
