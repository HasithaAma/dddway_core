﻿using System.Text;
using System.Linq;
using DDDWay.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDDWay.Domain
{
    /// <summary>
    /// Domain entity for the 
    /// </summary>
    public class Branch : EntityBase<int>
    {
        #region Domain logic

        
        #endregion

        #region ORM Fields

        public int BranchId { get; set; }

        public string Code { get; set; }

        protected override void ValidateAndThrowIfDomainException()
        {
            throw new System.NotImplementedException();
        }

        #endregion

    }

    public class BranchConfiguration : IEntityTypeConfiguration<Branch>
    {
        public void Configure(EntityTypeBuilder<Branch> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(x => x.BranchId);
        }
    }
}
