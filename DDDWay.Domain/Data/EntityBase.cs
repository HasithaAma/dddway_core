﻿using System.Collections.Generic;

namespace DDDWay.Domain
{
    public abstract class EntityBase<IdType>
    {
        private readonly List<BusinessRule> _brokenRules;

        public EntityBase()
        {
            this._brokenRules = new List<BusinessRule>();
        }

        //Make it other than public. If it is public, ORM will search for this entity
        protected List<BusinessRule> BrokenRules { get { return _brokenRules; } }

        //public IdType Id { get; set; }

        //Implement this for all deriving class
        protected abstract void ValidateAndThrowIfDomainException();

        protected void AddBrokenRule(BusinessRule businessRule)
        {
            _brokenRules.Add(businessRule);
        }

        public override bool Equals(object entity)
        {
            return entity != null
               && entity is EntityBase<IdType>
               && this == (EntityBase<IdType>)entity;
        }

        //public static bool operator ==(EntityBase<IdType> entity1, EntityBase<IdType> entity2)
        //{
        //    if ((object)entity1 == null && (object)entity2 == null)
        //    {
        //        return true;
        //    }

        //    if ((object)entity1 == null || (object)entity2 == null)
        //    {
        //        return false;
        //    }

        //    if (entity1.Id.ToString() == entity2.Id.ToString())
        //    {
        //        return true;
        //    }

        //    return false;
        //}

        //public static bool operator !=(EntityBase<IdType> entity1,
        //    EntityBase<IdType> entity2)
        //{
        //    return (!(entity1 == entity2));
        //}
    }
}
