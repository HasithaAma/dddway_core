﻿using System;

namespace DDDWay.Domain
{
    /// <summary>
    /// Every class must implement this interface. Contains id and audinting fields.
    /// </summary>
    
    public interface IEntityBase<IdType>
    {
        IdType Id { get; }
        DateTimeOffset CreatedDate { get; }
        DateTimeOffset ModifiedDate { get; }
        byte[] Version { get; }
    }
}