﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DDDWay.Domain
{
    /// <summary>
    /// Use for read only entities
    /// </summary>
    /// <typeparam name="T">Base entity</typeparam>
    /// <typeparam name="TId">Base type primary key data type. Allow it have as generic so any types of primary keys can be used.</typeparam>
	public interface IReadOnlyRepository<AggregateType, IdType> where AggregateType : IAggregateRoot
	{
        /// <summary>
        /// Find by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        AggregateType FindBy(IdType id);

        /// <summary>
        /// Find all
        /// </summary>
        /// <returns></returns>
		IEnumerable<AggregateType> FindAll();

        /// <summary>
        /// Find with predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IEnumerable<AggregateType> Find(Expression<Func<AggregateType, bool>> predicate);

        //Find with predicate and include related entities
        IQueryable<AggregateType> Fetch(Expression<Func<AggregateType, bool>> predicate, params Expression<Func<AggregateType, object>>[] includes);

    }
}
