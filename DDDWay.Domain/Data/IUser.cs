﻿namespace DDDWay.Domain
{
    public interface IUser : IEntityBase<int>
    {
        string UserName { get; set; }
        string Email { get; set; }
        string Title { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}
