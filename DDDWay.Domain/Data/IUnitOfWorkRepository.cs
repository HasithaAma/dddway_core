﻿namespace DDDWay.Domain
{
    public interface IUnitOfWorkRepository
	{
		void PersistInsertion(IAggregateRoot aggregateRoot);

		void PersistUpdate(IAggregateRoot aggregateRoot);

		void PersistDeletion(IAggregateRoot aggregateRoot);

	}
}
