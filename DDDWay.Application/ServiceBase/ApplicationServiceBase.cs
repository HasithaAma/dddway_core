﻿using System;
using DDDWay.Domain;

namespace DDDWay.Application
{
    public class ApplicationServiceBase
    {
        private readonly IUnitOfWork unitOfWork;

        public ApplicationServiceBase(IUnitOfWork _unitOfWork)
        {
            if (_unitOfWork == null) throw new ArgumentNullException("UnitOfWork");
            unitOfWork = _unitOfWork;
        }

        public IUnitOfWork UnitOfWork
        {
            get
            {
                return unitOfWork;
            }
        }
    }
}
