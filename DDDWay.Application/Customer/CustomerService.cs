﻿using System;
using DDDWay.Application.Messaging;
using DDDWay.Domain;
using System.Threading.Tasks;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;

namespace DDDWay.Application
{
    public class CustomerService : ApplicationServiceBase, ICustomerService

    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CustomerService(ICustomerRepository customerRepository, IMapper mapper, IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            this._customerRepository = customerRepository;
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<CustomerResponse> GetAllCustomers()
        {
            CustomerResponse getCustomerResponse = new CustomerResponse();
           
            var customers = _customerRepository.FindAll();
            var cust = new Customer();
            //cust.CustomerId = 1;
            var result = _mapper.Map<List<CustomerVm>>(customers);

            getCustomerResponse.Customers = result;

            return getCustomerResponse;

        }

        public void UpdateCustomer(CustomerRequest customer)
        {
            throw new NotImplementedException();
        }
    }
}
