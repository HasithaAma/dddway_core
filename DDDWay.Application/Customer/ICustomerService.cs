﻿using DDDWay.Application.Messaging;
using System.Threading.Tasks;

namespace DDDWay.Application
{
    public interface ICustomerService
    {
        Task<CustomerResponse> GetAllCustomers();

        void UpdateCustomer(CustomerRequest customer);
    }
}
