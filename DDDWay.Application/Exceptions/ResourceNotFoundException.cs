﻿using System.Net;
using DDDWay.Infrastructure.ExceptionHandling;

namespace DDDWay.Application.Exceptions
{
    public class ResourceNotFoundException : InternalException
    {
        public ResourceNotFoundException(string message)
            : base(message, HttpStatusCode.NotFound)
        { }
    }
}
