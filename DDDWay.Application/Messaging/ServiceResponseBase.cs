﻿using System;
using System.Net;

namespace DDDWay.Application.Messaging
{
    public abstract class ServiceResponseBase
    {
        public ServiceResponseBase()
        {
            this.Exception = null;
            this.Success = true;
            this.StatusCode = HttpStatusCode.OK;
        }

        public HttpStatusCode StatusCode { get; set; }

        public bool Success { get; set; }

        public int? ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        /// <summary>
		/// Save the exception thrown so that consumers can read it
		/// </summary>
		public Exception Exception { get; set; }
    }
}
