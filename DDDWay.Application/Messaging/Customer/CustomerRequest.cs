﻿namespace DDDWay.Application.Messaging
{
    public class CustomerRequest : ServiceRequestBase
    {
        public int CustomerId { get; set; }

        public string Code { get; set; }
    }
}
