﻿using System.Collections.Generic;
using DDDWay.Domain;

namespace DDDWay.Application.Messaging
{
    public class CustomerResponse: ServiceResponseBase
    {
        public List<CustomerVm> Customers { get; set; }
    }
}
