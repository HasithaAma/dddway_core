﻿using AutoMapper;
using DDDWay.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDDWay.Application
{
    public class CustomerProfile : AutoMapper.Profile
    {
        public CustomerProfile()
        {
            CreateMap<Customer, CustomerVm>();

            CreateMap<CustomerVm, Customer>();
        }
    }
}
