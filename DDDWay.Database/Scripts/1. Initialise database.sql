﻿if object_id ('[dbo].[Customer]', 'U') is not null  
	drop table [dbo].Customer
go

create table  [dbo].Customer
(
	CustomerId int identity(1,1) not null constraint pk_loginuser primary key,
	Code varchar(20) not null,
	CreatedBy int not null,
	CreatedDate datetimeoffset not null constraint DF_LoginUser_CreatedDate default SYSDATETIMEOFFSET(),
	ModifiedBy int not null,
	ModifiedDate datetimeoffset not null constraint DF_LoginUser_ModifiedDate default SYSDATETIMEOFFSET(),
	Version timestamp
)

GO