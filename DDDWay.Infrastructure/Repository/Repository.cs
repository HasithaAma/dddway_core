﻿using System;
using DDDWay.Domain;

namespace DDDWay.Infrastructure.Repository
{
    public class Repository<AggregateType, IdType> : ReadOnlyRepository<AggregateType, IdType>, IRepository<AggregateType, IdType> where AggregateType: class, IAggregateRoot
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly DDDWayDbContext dbContext;

        public Repository(IUnitOfWork _unitOfWork, DDDWayDbContext _dbContext)
            :base(_dbContext)
        {
            if (_unitOfWork == null) throw new ArgumentNullException("Unit of work");
            if (_dbContext == null) throw new ArgumentNullException("Object context factory");
            unitOfWork = _unitOfWork;
            dbContext = _dbContext;
        }

        public DDDWayDbContext DbContext
        {
            get
            {
                return dbContext;
            }

        }

        public void Delete(AggregateType aggregate)
        {
            throw new NotImplementedException();
        }

        public void Insert(AggregateType aggregate)
        {
            throw new NotImplementedException();
        }

        public void Update(AggregateType aggregate)
        {
            throw new NotImplementedException();
        }
    }
}
