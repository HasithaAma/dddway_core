﻿using System;
namespace DDDWay.Infrastructure.Repository
{
    public class User : EntityBase<int>
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PasswordHash { get; set; }
        public DateTimeOffset? LastLoginDate { get; set; }
        public DateTimeOffset? LastLogoutDate { get; set; }

        public override void Validate()
        {
            throw new NotImplementedException();
        }
    }

}
