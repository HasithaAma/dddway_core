﻿using System;
using System.Collections.Generic;
using DDDWay.Domain;

namespace DDDWay.Infrastructure.Repository
{
    public abstract class EntityBase<IdType>
    {
        private List<BusinessRule> _brokenRules = new List<BusinessRule>();

        //Navigational properties
        public int CreatedBy { get; set; }
        //public virtual IUser CreatedByLocal { get; set; }
        //public IUser CreatedBy { get { return CreatedByLocal; } set { CreatedByLocal = value; } }

        public DateTimeOffset CreatedDate { get; set; }

        //Navigational properties
        public int ModifiedBy { get; set; }
        //public virtual IUser ModifiedByLocal { get; set; }
        //public IUser ModifiedBy { get { return ModifiedByLocal; } set { ModifiedByLocal = value; } }

        public DateTimeOffset ModifiedDate { get; set; }

        public byte[] Version { get; set; }

        public abstract void Validate();

        public override bool Equals(object entity)
        {
            return entity != null
               && entity is EntityBase<IdType>
               && this == (EntityBase<IdType>)entity;
        }

        //public override int GetHashCode()
        //{
        //    return this.Id.GetHashCode();
        //}

        public static bool operator ==(EntityBase<IdType> entity1, EntityBase<IdType> entity2)
        {
            if ((object)entity1 == null && (object)entity2 == null)
            {
                return true;
            }

            if ((object)entity1 == null || (object)entity2 == null)
            {
                return false;
            }

            //if (entity1.Id.ToString() == entity2.Id.ToString())
            //{
            //    return true;
            //}

            return false;
        }

        public static bool operator !=(EntityBase<IdType> entity1,
            EntityBase<IdType> entity2)
        {
            return (!(entity1 == entity2));
        }
    }

}
