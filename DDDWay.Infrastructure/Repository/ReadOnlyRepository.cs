﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using DDDWay.Domain;

namespace DDDWay.Infrastructure.Repository
{
    public class ReadOnlyRepository<AggregateType, IdType> : IReadOnlyRepository<AggregateType, IdType> where AggregateType : class,IAggregateRoot
    {
        private readonly DDDWayDbContext _dbContext;

        public ReadOnlyRepository(DDDWayDbContext dbContext)
        {
            if (dbContext == null) throw new ArgumentNullException("Object context factory");

            this._dbContext = dbContext;
        }

        public IEnumerable<AggregateType> FindAll()
        {
            return this._dbContext.Set<AggregateType>().AsEnumerable();
        }

        public IEnumerable<AggregateType> Find(Expression<Func<AggregateType, bool>> predicate)
        {
            return _dbContext.Set<AggregateType>().Where(predicate).AsEnumerable();
        }

        public AggregateType FindBy(IdType id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<AggregateType> Fetch(Expression<Func<AggregateType, bool>> predicate, params Expression<Func<AggregateType, object>>[] includes)
        {
            IQueryable<AggregateType> query = _dbContext.Set<AggregateType>();
            var parameter = Expression.Parameter(typeof(AggregateType));

            foreach (var include in includes)
                query = query.Include(include);

            if (predicate != null)
                return query.Where(predicate);
            else
                return query;
        }
    }
}