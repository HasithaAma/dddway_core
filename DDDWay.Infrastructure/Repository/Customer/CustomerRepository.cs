﻿using DDDWay.Domain;

namespace DDDWay.Infrastructure.Repository
{
    public class CustomerRepository : Repository<Customer, int>, ICustomerRepository
    {
        public CustomerRepository(IUnitOfWork unitOfWork, DDDWayDbContext dbContext)
            : base(unitOfWork, dbContext)
        { }

    }


}
