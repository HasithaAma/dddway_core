﻿using System;
using System.Linq;
using System.Reflection;
using DDDWay.Domain;
using Microsoft.EntityFrameworkCore;

namespace DDDWay.Infrastructure.Repository
{

    public class DDDWayDbContext : DbContext
    {
        public DDDWayDbContext(DbContextOptions<DDDWayDbContext> options)
          : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Add Configuration classes to the model
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes().Where(tp => tp.Namespace != null && tp.GetInterfaces().Any(p => p.Name.Contains("IEntityTypeConfiguration")));

            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.ApplyConfiguration(configurationInstance);
            }

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Branch> Customer { get; set; }
    }
}
