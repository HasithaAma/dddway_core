﻿using System;
using System.Net;

namespace DDDWay.Infrastructure.ExceptionHandling
{
    public class InternalException : Exception
    {
        public InternalException()
        { }

        public InternalException(string message)
            : base(message)
        { }

        public InternalException(string message, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
            : base(message)
        {
            this.StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; }
    }
}
