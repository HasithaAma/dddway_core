﻿using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System.Reflection;
using System;

namespace DDDWay.Infrastructure.ExceptionHandling
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        /// <summary>
        /// Top level exception handler, generates a response with the appropriate status code
        /// </summary>
        /// <param name="context"></param>
        public void OnException(ExceptionContext context)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError;
            string message = string.Empty;

            if (context.Exception.GetType().GetTypeInfo().IsSubclassOf(typeof(InternalException)))
            {
                status = ((InternalException)context.Exception).StatusCode;
                message = ((InternalException)context.Exception).Message;
            }
            else
                message = ((Exception)context.Exception).Message;

            context.ExceptionHandled = true;

            HttpResponse response = context.HttpContext.Response;
            response.StatusCode = (int)status;
            response.ContentType = "application/json";

            response.WriteAsync(JsonConvert.SerializeObject(message));
        }
    }
}
